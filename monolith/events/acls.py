from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests


def get_photo_url(query):
    # create a dictionary for the headers to use in the request
    # create the URL for the request with the city and the state
    # make the request
    # Parse the JSON response
    # return a dict that contains a 'picture_url' key and
    # one of the URLs for one of the pictures in the response

    headers = {"Authorization": PEXELS_API_KEY}

    url = f"https://api.pexels.com/v1/search?query={query}"

    response = requests.get(url, headers=headers)  # brings back the request as python
    api_dict = response.json()  # takes that python and turns it into a json string

    # browsers can only read in string and binary !!!

    return api_dict["photos"][0]["src"]["original"]


def get_weather_url(query):
    '''
     Create the URL for the geocoding API with the city and state
     Make the request
     Parse the JSON response
     Get the latitude and longitude from the response

     Create the URL for the current weather API with the latitude
       and longitude
    Make the request
    Parse the JSON response
    Get the main temperature and the weather's description and put
        them in a dictionary
        return the dictionary

    '''
    headers = {"Authorization": OPEN_WEATHER_API_KEY}

    url = 'http://api.openweathermap.org/geo/1.0/direct?q={query}'

    response = requests.get(url, headers=headers)

    latitude = response.json([0]["lat"])
    longitude = response.json([0]["lon"])

    parameters = {
        "longitude": longitude,
        "latitude": latitude,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
        }
    #  ^^ the parameters that are required must be in order the api request url commands

    weather_url = 'https://api.openweathermap.org/data/2.5/weather'

    response = requests.get(
        weather_url, headers=headers, parameters=parameters
        )

    api_dict = response.json()

    return api_dict["main"]["temp"]
